require './model/word'
require './controller/word_controller'

class StateCreateWord < StateConsole
  def show_screen()
    super()
    wordController = WordController.new()
    puts("\t\t\tCrie uma nova palavra!\n\n")
    
    while(true)
      print("Digite a palavra: ")
      @word = gets.to_s.strip()
      if(@word.length <=0)
        puts("A palavra deve ser digitada!")
      elsif(@word == "exit")
        return StateMenu
      elsif(!wordController.select_word(@word).nil?)
        puts("Essa palavra já está na base de dados!")
      else
        break
      end
    end

    while(true)
      print( "Digite a categoria da palavra: ")
      @category = gets.to_s.strip()
      if(@category.length <= 0)
        puts("A categoria deve ser digitada!")
      else
        break
      end
    end

    while true
      print("Escolha a dificuldade (1- Fácil, 2 - Normal, 3 - Díficil): ")
      @difficulty = gets.strip().to_i
      if(@difficulty < 1 || @difficulty > 3)
        puts "Escolha uma das dificuldades disponíveis!"
      else
        break
      end
    end

    newWord = Word.new @word, @difficulty, @category
    puts(wordController.add_word(newWord))
    gets
    
    return StateMenu
  end
end