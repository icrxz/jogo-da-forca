class StateRanking < StateConsole
  def show_screen()
    super()
    playerController = PlayerController.new()

    puts("\t\t\tRanking\n\n")
    puts("Id\tNome\t\tIdade\tScore")
    list = playerController.list_players()
    list.each do |val|
      puts("#{val.id}\t#{val.name}\t#{val.age}\t#{val.score}")
    end
    gets
    return StateMenu
  end
end