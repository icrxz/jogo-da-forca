require_relative 'stateConsole'
require_relative '../lib/singleton_player_logged'

class StateLogin < StateConsole
  def show_screen()
    super()
    puts("\t\t\tSeja bem vindo ao Jogo da velha!\n\n")
    print("Digite seu login: ")
    @login = gets.to_s.strip()
    @player = PlayerController.new().select_player(@login)
    if @player.nil?
      return StateCreateUser
    else
      SingletonPlayerLogged.instance.playerLogged = @player
      return StateMenu
    end
  end
end

