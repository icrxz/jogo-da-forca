class StateMenu < StateConsole
  def initialize()
    @player = SingletonPlayerLogged.instance.playerLogged
  end

  def show_screen()
    super()
    puts("\t\t\t\tMenu Principal\n\nSeja bem vindo, #{@player.name}!\n\n")
    
    print("1 - Jogo
2 - Ranking
3 - Criar Usuário
4 - Criar Palavra
5 - Deslogar
6 - Sair\n
Digite a opção escolhida: ")
    
    ope = gets.to_i

    case ope
    when 1
      return StateGame
    when 2
      return StateRanking
    when 3
      return StateCreateUser
    when 4
      return StateCreateWord
    when 5
      return StateLogin
    when 6
      exit
    else
      puts("\nOpção não encontrada, digite novamente!")
      gets
      return show_screen()
    end
  end
end