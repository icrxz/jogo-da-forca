require './model/player'
require './controller/player_controller'

class StateCreateUser < StateConsole
  def show_screen()
    super()
    playerController = PlayerController.new()
    puts("\t\t\tCrie um novo usuário!\n\n")
    
    while(true)
      print("Digite seu login: ")
      @login = gets.to_s.strip()
      if(@login.length <=0)
        puts("O login deve ser digitado!")
      elsif(@login == "exit")
        return quit_screen()
      elsif(!playerController.select_player(@login).nil?)
        puts("Já existe esse login, digite um diferente!")
      else
        break
      end
    end

    while(true)
      print("Digite seu nome: ")
      @name = gets.to_s.strip()
      if(@name.length <=0)
        puts("O nome deve ser digitado!")
      else
        break
      end
    end
    
    while(true)
      print("Digite seu sexo (M/F): ")
      @gender = gets.to_s.strip()[0]
      if(@gender.downcase() != 'm' && @gender.downcase() != 'f')
        puts("O sexo deve ser 'M' ou 'F'!")
      else
        break
      end
    end

    while(true)
      print("Digite sua idade: ")
      @age = gets.strip().to_i
      if(@age <= 0)
        puts("A idade não pode ser negativa!")
      else
        break
      end
    end

    player = Player.new(@login, @name, @gender, @age)
    puts playerController.add_player(player)
    gets()
    
    return quit_screen()
  end

  private def quit_screen()
    if(SingletonPlayerLogged.instance.playerLogged.nil?)
      return StateLogin
    else
      return StateMenu
    end
  end
end