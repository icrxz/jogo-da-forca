require_relative "../model/enum/difficulty"

class StateGame < StateConsole
  
  def initialize()
    @player = SingletonPlayerLogged.instance.playerLogged
    @wordController = WordController.new()
    @count = 0
    @lineHead = "\t|\t "
    @lineUpBody = "\t|\t"
    @lineBody = "\t|\t "
    @lineLegs = "\t|\t"
    @word = @wordController.sort_word()
    @hiddenWord = ""
    @keysPressed = ""

    @word.word.split('').each do
      @hiddenWord += " _"
    end
  end
  
  def show_screen()
    super()
    puts("\t\t\t\tJogo da Forca\n\n")
    puts("Categoria: #{@word.category.capitalize()}")
    puts("Dificuldade: #{Difficulty.new().status[@word.difficulty.to_i]}")
    print("\t----------" + "\n" +
          "\t|        |" + "\n" +
          @lineHead + "\n" +
          @lineUpBody + "\n" +
          @lineBody + "\n" +
          @lineLegs + "\n" +
          "\t|" + "\n" +
          "     -------\t\t" + @hiddenWord + "\n\n" +
          @keysPressed + "\nDigite uma letra: ");
     charPressed = gets[0]
     if verify_word(charPressed)
      return StateMenu
     end
     show_screen()
  end

  private 
  
  def verify_word(charPressed)
    if !@keysPressed.include?(charPressed)
      @keysPressed += charPressed
      if(@word.word.include?(@keysPressed[-1]))
        (0...@word.word.length).each do |x|
          if @word.word[x] == @keysPressed[-1]
            @hiddenWord[1+x*2] = @keysPressed[-1]
            if !@hiddenWord.include?("_")
              puts("Você ganhou!")
              @player.score += 1
              puts "Nome: #{@player.name} Score: #{@player.score}"
              gets
              return true
            end
          end
        end
      else
        case @count
        when 0
          @lineHead += "O"
        when 1
          @lineUpBody += "/"
        when 2
          @lineUpBody += "|"
        when 3
          @lineUpBody += "\\"
        when 4
          @lineBody += "|"
        when 5
          @lineLegs += "/ "
        when 6
          @lineLegs += "\\"
        when 7
          puts "Você perdeu!"
          gets
          return true
        end
        @count += 1
      end
    end
    return false
  end
end