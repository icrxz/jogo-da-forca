require_relative 'base'

class Word < Base
  
  attr_accessor :word, :difficulty, :category

  def initialize(word, difficulty, category, dateModified = Time.now(), dateCreated = Time.now(), id = 0)
    super(dateCreated, dateModified, id)
    @word = word
    @difficulty = difficulty
    @category = category
  end
end