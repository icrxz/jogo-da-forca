class Base
  attr_accessor :dateModified
  attr_reader :id, :dateCreated

  def initialize(dateCreated = Time.now, dateModified = Time.now, id = 0)
    @id = id
    @dateCreated = dateCreated
    @dateModified = dateModified
  end
end