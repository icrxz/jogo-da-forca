require_relative 'base'

class Player < Base

  attr_accessor :login, :name, :gender, :age, :score

  def initialize(login, name, gender, age, dateModified = Time.now, dateCreated = Time.now, id = 0, score = 0)
    super(dateCreated, dateModified, id)
    @login = login
    @name = name    
    @gender = gender
    @age = age
    @score = score
  end
end