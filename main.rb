require_relative 'view/stateConsole'
require_relative 'view/stateCreateUser'
require_relative 'view/stateCreateWord'
require_relative 'view/stateGame'
require_relative 'view/stateLogin'
require_relative 'view/stateMenu'
require_relative 'view/stateRanking'

class Main
  attr_accessor :state
  

  def initialize(state = StateLogin.new)
    @state = state
  end

  def run()
    while(true)
      begin
        if(@state.is_a? StateConsole)
          @state = @state.show_screen()
          @state = @state.new()
        else
          exit
        end
      rescue SystemExit => e
        puts("Programa finalizando!")
        exit
      rescue Exception => e
        puts(e.message)
        gets
      end
    end
  end
end

Main.new().run()