require 'rspec'
require_relative "../main"

RSpec.describe Main do
  describe "#run" do 
    let(:main_incorrect) {described_class.new(1)}
    let(:main_correct) {described_class.new()}
    before do 
      allow(main_correct.state).to receive(:show_screen).and_return(Object)
    end

    it "exits with an incorrect state" do
      expect{main_incorrect.run()}.to raise_error(SystemExit)
    end

    it "exits with an correct state" do
      expect{main_correct.run()}.to raise_error(SystemExit)
    end
  end
end