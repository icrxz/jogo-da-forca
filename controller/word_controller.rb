require_relative 'base_controller'
require_relative 'db_connect'
require 'pg'

class WordController < BaseController

  def initialize()
    super("Word", 5)
  end

  def add_word(word)
    @word = word
    
    dbCon = DbConnect.new()
    con = dbCon.get_connection()

    con.transaction do |con|
      con.prepare('stm1', "INSERT INTO Word VALUES (DEFAULT, $1, $2, $3, $4, $5);")
      con.exec_prepared('stm1', [@word.word, @word.difficulty, @word.category, @word.dateCreated, @word.dateModified])
    end
    
    return "Palavra cadastrada com sucesso!"
  end

  def select_word(word)
    @word = word

    dbCon = DbConnect.new()
    con = dbCon.get_connection()
    rs = ""
    con.transaction do |con|
      con.prepare('stm1', "SELECT * FROM Word WHERE word = $1;")
      rs = con.exec_prepared('stm1', [@word])
    end
    
    if(rs.ntuples <= 0)
      return nil
    else
      return create_word(rs[0])
    end
  end

  def identify_word(id)
    @id = id
    dbCon = DbConnect.new()
    con = dbCon.get_connection()
    rs = ""
    con.transaction do |con|
      con.prepare('stm1', "SELECT * FROM Word WHERE id = $1;")
      rs = con.exec_prepared('stm1', [@id])
    end

    if(rs.ntuples <= 0)
      puts"Não retornou objeto"
      return nil
    else
      return create_word(rs[0])
    end
  end

  private def create_word(word)
    if(word.length <= 0)
      return nil
    end
    _word = Word.new(word["word"], word["difficulty"], word["category"], word["datemodified"], word["datecreated"], word["id"])
    return _word
  end

  def sort_word()
    num = rand(1..table_size())
    word = identify_word(num)
    if(word.nil?)
      sort_word()
    else
      return word
    end
  end
end