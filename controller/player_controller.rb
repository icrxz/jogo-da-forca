require_relative 'base_controller'
require_relative 'db_connect'
require 'pg'

class PlayerController < BaseController

  def initialize()
    super("Player", 7)
  end

  def add_player(_player)
    player = _player
    
    dbCon = DbConnect.new()
    con = dbCon.get_connection()

    con.transaction do |con|
      con.prepare('stm1', "INSERT INTO #{@table} VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7);")
      con.exec_prepared('stm1', [player.name, player.login, player.gender, player.age, player.score, player.dateCreated, player.dateModified])
    end
    con.close()
    return "Usuário cadastrado com sucesso!"
  end

  def select_player(_login)
    login = _login

    dbCon = DbConnect.new()
    con = dbCon.get_connection()
    rs = ""
    con.transaction do |con|
      con.prepare('stm1', "SELECT * FROM Player WHERE login = $1;")
      rs = con.exec_prepared('stm1', [login])
    end
    con.close()
    if(rs.ntuples <= 0)
      return nil
    else
      return create_player(rs[0])
    end
  end

  def list_players()
    dbCon = DbConnect.new()
    con = dbCon.get_connection()
    rs = ""
    listPlayer = []
    
    rs = con.exec("SELECT * FROM #{@table};")
    
    if( rs.ntuples <= 0)
      return nil
    else
      (0...rs.ntuples()).each do |x|
        listPlayer.push(create_player(rs[x]))
      end
    end
    con.close()
    return listPlayer
  end

  private def create_player(_player)
    player = _player
    if(player.length <= 0)
      return nil
    end
    pl = Player.new(player["login"], player["name"], player["gender"], player["age"].to_i, player["datemodified"], player["datecreated"], player["id"].to_i, player["score"].to_i)
    return pl
  end

end